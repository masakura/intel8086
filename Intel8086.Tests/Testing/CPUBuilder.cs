using System;
using System.Collections.Generic;
using Intel8086.Binary;
using Intel8086.Memory;
using Intel8086.Register;

namespace Intel8086.Testing;

internal sealed class CPUBuilder
{
    private readonly CPU _cpu = new();

    public CPUBuilder DataSegment(UnsignedWord segment, IEnumerable<byte> bytes)
    {
        return Segment(_cpu.Registers.DS, segment, bytes);
    }

    public CPUBuilder StackSegment(UnsignedWord segment, IEnumerable<byte> bytes)
    {
        return Segment(_cpu.Registers.SS, segment, bytes);
    }

    public CPUBuilder Program(IEnumerable<byte> bytes)
    {
        return Segment(_cpu.Registers.CS, _cpu.Registers.CS.Value, bytes);
    }

    
    private CPUBuilder Segment(SegmentRegister segmentRegister, UnsignedWord segment, IEnumerable<byte> bytes)
    {
        segmentRegister.Value = segment;
        _cpu.Memory.Poke(new Address(segmentRegister, 0), bytes);
        return this;
    }

    public CPUBuilder Registers(Action<Registers> configure)
    {
        configure(_cpu.Registers);
        return this;
    }

    public CPU Build()
    {
        return _cpu;
    }
}