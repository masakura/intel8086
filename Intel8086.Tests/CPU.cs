﻿using Intel8086.Memory;
using Intel8086.Operations;
using Intel8086.Register;

namespace Intel8086;

public sealed class CPU
{
    private readonly ProgramMemory _programMemory;

    public CPU()
    {
        _programMemory = new ProgramMemory(Registers, Memory);
    }

    public CPUMemory Memory { get; } = new();
    public Registers Registers { get; } = new();
    private Parser Parser { get; } = new();

    public void RunOne()
    {
        var operation = Parser.Parse(_programMemory);
        operation.Run(this);
    }
}