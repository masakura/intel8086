﻿using Intel8086.Binary;
using Intel8086.Store;

namespace Intel8086.Register;

public abstract class RegisterWord : IRegister<UnsignedWord>
{
    protected RegisterWord()
    {
    }

    protected RegisterWord(UnsignedWord value)
    {
        Value = value;
    }

    public UnsignedWord Value { get; set; }

    UnsignedWord ISource<UnsignedWord>.GetValue(CPU cpu)
    {
        return Value;
    }

    public override string ToString()
    {
        return Value.ToString();
    }
}