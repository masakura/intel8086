using Intel8086.Binary;
using Intel8086.Store;

namespace Intel8086.Register;

public sealed class GenericRegisterByte : IRegister<UnsignedByte>
{
    private readonly GenericRegisterWord _register;
    private readonly IRegisterAdapter _registerAdapter;

    private GenericRegisterByte(GenericRegisterWord register, IRegisterAdapter registerAdapter)
    {
        _register = register;
        _registerAdapter = registerAdapter;
    }

    public UnsignedByte Value
    {
        get => _registerAdapter.Value(_register);
        set => _registerAdapter.Value(_register, value);
    }

    public static GenericRegisterByte High(GenericRegisterWord register)
    {
        return new GenericRegisterByte(register, HighRegisterAdapter.Instance);
    }

    public static GenericRegisterByte Low(GenericRegisterWord register)
    {
        return new GenericRegisterByte(register, LowRegisterAdapter.Instance);
    }

    private interface IRegisterAdapter
    {
        UnsignedByte Value(GenericRegisterWord word);
        void Value(GenericRegisterWord word, UnsignedByte value);
    }

    private sealed class HighRegisterAdapter : IRegisterAdapter
    {
        private HighRegisterAdapter()
        {
        }

        public static IRegisterAdapter Instance { get; } = new HighRegisterAdapter();

        public UnsignedByte Value(GenericRegisterWord word)
        {
            return word.Value.High();
        }

        public void Value(GenericRegisterWord word, UnsignedByte value)
        {
            word.Value = word.Value.High(value);
        }
    }

    private sealed class LowRegisterAdapter : IRegisterAdapter
    {
        private LowRegisterAdapter()
        {
        }

        public static IRegisterAdapter Instance { get; } = new LowRegisterAdapter();

        public UnsignedByte Value(GenericRegisterWord word)
        {
            return word.Value.Low();
        }

        public void Value(GenericRegisterWord word, UnsignedByte value)
        {
            word.Value = word.Value.Low(value);
        }
    }

    UnsignedByte ISource<UnsignedByte>.GetValue(CPU cpu)
    {
        return Value;
    }
}