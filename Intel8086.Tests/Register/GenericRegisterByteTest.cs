using Intel8086.Binary;
using PowerAssert;
using Xunit;

namespace Intel8086.Register;

public sealed class GenericRegisterByteTest
{
    private readonly GenericRegisterWord _target;

    public GenericRegisterByteTest()
    {
        _target = new GenericRegisterWord {Value = 0x0123};
    }

    [Fact]
    public void TestHigh()
    {
        PAssert.IsTrue(() => _target.High.Value == new UnsignedByte(0x01));
    }

    [Fact]
    public void TestSetHigh()
    {
        _target.High.Value = 0xfe;

        PAssert.IsTrue(() => _target.Value == new UnsignedWord(0xfe23));
    }

    [Fact]
    public void TestLow()
    {
        PAssert.IsTrue(() => _target.Low.Value == new UnsignedByte(0x23));
    }
}