﻿using Intel8086.Binary;

namespace Intel8086.Register;

public sealed class GenericRegisterWord : RegisterWord
{
    public GenericRegisterWord():this(UnsignedWord.Zero)
    {
    }

    private GenericRegisterWord(UnsignedWord value) : base(value)
    {
        High = GenericRegisterByte.High(this);
        Low = GenericRegisterByte.Low(this);
    }

    public GenericRegisterByte High { get; }
    public GenericRegisterByte Low { get; }

    public void Add(UnsignedWord value)
    {
        Value = Value.Add(value);
    }
}