﻿namespace Intel8086.Register;

public sealed class PointerRegister : RegisterWord
{
    public void Increment()
    {
        Value = Value.Increment();
    }
}