﻿using Intel8086.Binary;

namespace Intel8086.Register;

public sealed class SegmentRegister : RegisterWord
{
    public SegmentRegister() : base(0)
    {
    }

    public SegmentRegister(UnsignedWord value) : base(value)
    {
    }

    public uint AddressBase()
    {
        return (uint) Value.ToUInt16() * 0x10;
    }
}