﻿namespace Intel8086.Register;

public sealed class Registers
{
    public Registers()
    {
        AH = GenericRegisterByte.High(AX);
        AL = GenericRegisterByte.Low(AX);
        BH = GenericRegisterByte.High(BX);
        BL = GenericRegisterByte.Low(BX);
        CH = GenericRegisterByte.High(CX);
        CL = GenericRegisterByte.Low(CX);
        DH = GenericRegisterByte.High(DX);
        DL = GenericRegisterByte.Low(DX);
    }

    public GenericRegisterWord AX { get; } = new();
    public GenericRegisterWord BX { get; } = new();
    public GenericRegisterWord CX { get; } = new();
    public GenericRegisterWord DX { get; } = new();

    public GenericRegisterByte AH { get; }
    public GenericRegisterByte AL { get; }
    public GenericRegisterByte BH { get; }
    public GenericRegisterByte BL { get; }
    public GenericRegisterByte CH { get; }
    public GenericRegisterByte CL { get; }
    public GenericRegisterByte DH { get; }
    public GenericRegisterByte DL { get; }

    public GenericRegisterWord SP { get; } = new();
    public GenericRegisterWord BP { get; } = new();
    public GenericRegisterWord SI { get; } = new();
    public GenericRegisterWord DI { get; } = new();
    public PointerRegister PC { get; } = new();

    public SegmentRegister CS { get; } = new(0xffff);
    public SegmentRegister DS { get;  } = new();
    public SegmentRegister SS { get; } = new();
}