using Intel8086.Store;

namespace Intel8086.Register;

public interface IRegister<T> : ISource<T>
{
    public T Value { get; set; }
}