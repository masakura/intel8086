namespace Intel8086.Store;

/**
 * data source. (memory or register)
 */
public interface ISource<out T>
{
    public T GetValue(CPU cpu);
}