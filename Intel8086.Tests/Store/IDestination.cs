﻿namespace Intel8086.Store;

public interface IDestination<in T>
{
    public void SetValue(CPU cpu, T value);
}