﻿namespace Intel8086.Store;

public sealed class ImmediateSource<T> : ISource<T>
{
    private readonly T _value;

    public ImmediateSource(T value)
    {
        _value = value;
    }

    public T GetValue(CPU cpu)
    {
        return _value;
    }
}