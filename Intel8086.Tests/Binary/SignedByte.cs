﻿using System;

namespace Intel8086.Binary;

public readonly struct SignedByte : IEquatable<SignedByte>
{
    private readonly sbyte _value;

    public SignedByte(sbyte value)
    {
        _value = value;
    }

    public SignedByte(byte value) : this((sbyte)value)
    {
    }

    public static implicit operator SignedByte(sbyte value)
    {
        return new SignedByte(value);
    }

    public bool Equals(SignedByte other)
    {
        return _value == other._value;
    }

    public override bool Equals(object? obj)
    {
        return obj is SignedByte other && Equals(other);
    }

    public override int GetHashCode()
    {
        return _value.GetHashCode();
    }

    public static bool operator ==(SignedByte left, SignedByte right)
    {
        return left.Equals(right);
    }

    public static bool operator !=(SignedByte left, SignedByte right)
    {
        return !left.Equals(right);
    }

    public sbyte ToSByte() => _value;
}