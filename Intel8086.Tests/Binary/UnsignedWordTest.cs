using PowerAssert;
using Xunit;

namespace Intel8086.Binary;

public sealed class UnsignedWordTest
{
    [Fact]
    public void TestCreateFromByte()
    {
        var actual = new UnsignedWord(new UnsignedByte(0x12), new UnsignedByte(0x34));

        PAssert.IsTrue(() => actual == 0x1234);
    }

    [Theory]
    [InlineData((ushort) 0x0123, (byte) 0x01)]
    [InlineData((ushort) 0xfe00, (byte) 0xfe)]
    public void TestHigh(ushort source, byte expected)
    {
        UnsignedWord target = source;

        PAssert.IsTrue(() => target.High() == expected);
    }

    [Fact]
    public void TestWithHigh()
    {
        var actual = new UnsignedWord(0x0123).High(0xfe);

        PAssert.IsTrue(() => actual == new UnsignedWord(0xfe23));
    }

    [Theory]
    [InlineData((ushort) 0x0123, (byte) 0x23)]
    [InlineData((ushort) 0x1efe, (byte) 0xfe)]
    public void TestLow(ushort source, byte expected)
    {
        UnsignedWord target = source;

        PAssert.IsTrue(() => target.Low() == expected);
    }

    [Fact]
    public void TestWithLow()
    {
        var actual = new UnsignedWord(0x0123).Low(0xfe);

        PAssert.IsTrue(() => actual == new UnsignedWord(0x01fe));
    }
}