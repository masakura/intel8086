﻿using System;

namespace Intel8086.Binary;

public readonly struct UnsignedByte : IEquatable<UnsignedByte>
{
    private readonly byte _value;

    public UnsignedByte(byte value)
    {
        _value = value;
    }

    public byte ToByte()
    {
        return _value;
    }

    public ushort ToUInt16()
    {
        return _value;
    }

    public UnsignedByte And(UnsignedByte value)
    {
        return new UnsignedByte((byte) (_value & value._value));
    }

    public UnsignedByte Or(UnsignedByte value)
    {
        return new UnsignedByte((byte)(_value | value._value));
    }

    public override string ToString()
    {
        return $"{_value:x02}";
    }

    public bool Equals(UnsignedByte other)
    {
        return _value == other._value;
    }

    public override bool Equals(object? obj)
    {
        return obj is UnsignedByte other && Equals(other);
    }

    public override int GetHashCode()
    {
        return _value.GetHashCode();
    }

    public static bool operator ==(UnsignedByte left, UnsignedByte right)
    {
        return left.Equals(right);
    }

    public static bool operator !=(UnsignedByte left, UnsignedByte right)
    {
        return !left.Equals(right);
    }

    public static UnsignedByte operator <<(UnsignedByte value, int count)
    {
        return (UnsignedByte) (value._value << count);
    }

    public static UnsignedByte operator >> (UnsignedByte value, int count)
    {
        return (UnsignedByte) (value._value >> count);
    }

    public static UnsignedByte operator &(UnsignedByte left, UnsignedByte right)
    {
        return (UnsignedByte) (left._value & right._value);
    }

    public static UnsignedByte operator |(UnsignedByte left, UnsignedByte right)
    {
        return (UnsignedByte) (left._value | right._value);
    }

    public static implicit operator UnsignedByte(byte value)
    {
        return new UnsignedByte(value);
    }

    public SignedByte Signed()
    {
        return new SignedByte(_value);
    }
}