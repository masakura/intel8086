﻿using System;

namespace Intel8086.Binary;

public readonly struct UnsignedWord : IEquatable<UnsignedWord>
{
    private readonly ushort _value;

    public UnsignedWord(UnsignedByte high, UnsignedByte low) : this(Combine(high, low))
    {
    }

    public UnsignedWord(ushort value)
    {
        _value = value;
    }

    public static UnsignedWord Zero { get; } = new(0);

    public ushort ToUInt16()
    {
        return _value;
    }

    public override string ToString()
    {
        return $"{_value:x04}";
    }

    public UnsignedWord Add(UnsignedWord value)
    {
        return new UnsignedWord((ushort)(_value + value._value));
    }

    public bool Equals(UnsignedWord other)
    {
        return _value == other._value;
    }

    public override bool Equals(object? obj)
    {
        return obj is UnsignedWord other && Equals(other);
    }

    public override int GetHashCode()
    {
        return _value.GetHashCode();
    }

    public static bool operator ==(UnsignedWord left, UnsignedWord right)
    {
        return left.Equals(right);
    }

    public static bool operator !=(UnsignedWord left, UnsignedWord right)
    {
        return !left.Equals(right);
    }

    public static implicit operator UnsignedWord(ushort value)
    {
        return new UnsignedWord(value);
    }

    public static UnsignedWord operator +(UnsignedWord left, UnsignedWord right)
    {
        return new UnsignedWord((ushort)(left._value + right._value));
    }

    public UnsignedWord Increment()
    {
        return new UnsignedWord((ushort) (_value + 1));
    }

    public UnsignedByte High()
    {
        return new UnsignedByte((byte) (_value >> 0x08));
    }

    public UnsignedWord High(UnsignedByte value)
    {
        return new UnsignedWord((ushort) ((value.ToByte() << 0x08) | Low().ToByte()));
    }

    public UnsignedByte Low()
    {
        return new UnsignedByte((byte) (_value & 0xff));
    }

    public UnsignedWord Low(UnsignedByte value)
    {
        return new UnsignedWord((ushort) ((High().ToByte() << 0x08) | value.ToByte()));
    }

    private static ushort Combine(UnsignedByte high, UnsignedByte low)
    {
        return (ushort) ((ushort) (high.ToUInt16() << 8) | low.ToUInt16());
    }
}