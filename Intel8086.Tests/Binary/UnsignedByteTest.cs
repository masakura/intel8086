using PowerAssert;
using Xunit;

namespace Intel8086.Binary;

public sealed class UnsignedByteTest
{
    private readonly UnsignedByte _target;

    public UnsignedByteTest()
    {
        _target = new UnsignedByte(0b1011_0110);
    }

    [Fact]
    public void TestShiftLeftMovMemoryToRegisterWordParserOperatorOverload()
    {
        var actual = _target << 4;

        PAssert.IsTrue(() => actual == 0b0110_0000);
    }

    [Fact]
    public void TestShiftRightOperatorOverload()
    {
        var actual = _target >> 4;

        PAssert.IsTrue(() => actual == 0b0000_1011);
    }

    [Fact]
    public void TestAndOverload()
    {
        var actual = _target & 0b1000_1111;

        PAssert.IsTrue(() => actual == 0b1000_0110);
    }

    [Fact]
    public void TestOrOverload()
    {
        var actual = _target | 0b0101_0000;

        PAssert.IsTrue(() => actual == 0b1111_0110);
    }


    [Theory]
    [InlineData((byte)0, (sbyte)0)]
    [InlineData((byte)1, (sbyte)1)]
    [InlineData((byte)127, (sbyte)127)]
    [InlineData((byte)128, (sbyte)-128)]
    [InlineData((byte)255, (sbyte)-1)]
    public void TestSigned(byte b, sbyte expected)
    {
        var target = new UnsignedByte(b);

        var actual = target.Signed();

        PAssert.IsTrue(() => actual == expected);
    }
}