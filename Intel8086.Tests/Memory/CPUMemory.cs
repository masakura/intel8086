﻿using System.Collections.Generic;
using System.Linq;
using Intel8086.Binary;
using Intel8086.Store;

namespace Intel8086.Memory;

public sealed class CPUMemory
{
    private readonly UnsignedByte[] _memory;

    public CPUMemory() : this(new byte[1024 * 1024])
    {
    }

    // ReSharper disable once ParameterTypeCanBeEnumerable.Local
    public CPUMemory(byte[] memory)
    {
        _memory = memory.Select(v => new UnsignedByte(v)).ToArray();
    }

    public UnsignedByte PeekByte(Address address)
    {
        return _memory[address.ToUInt()];
    }

    public UnsignedWord PeekWord(Address address)
    {
        return new UnsignedWord(PeekByte(address.Increment()), PeekByte(address));
    }

    private void Poke(Address address, UnsignedByte value)
    {
        _memory[address.ToUInt()] = value;
    }

    public void Poke(Address address, IEnumerable<byte> bytes)
    {
        foreach (var b in bytes)
        {
            Poke(address, b);
            address = address.Increment();
        }
    }

    public MemoryCellWord Cell(Address address)
    {
        return new MemoryCellWord(address);
    }
}

public class MemoryCellWord : ISource<UnsignedWord>
{
    private readonly Address _address;

    public MemoryCellWord(Address address)
    {
        _address = address;
    }

    public UnsignedWord GetValue(CPU cpu)
    {
        return cpu.Memory.PeekWord(_address);
    }
}