using PowerAssert;
using Xunit;

namespace Intel8086.Memory;

public sealed class CPUMemoryTest
{
    private readonly CPUMemory _target;

    public CPUMemoryTest()
    {
        _target = new CPUMemory(new byte[]{0x34, 0x12, 0x78, 0x56});
    }

    [Fact]
    public void TestPeekByte()
    {
        var actual = _target.PeekByte(0x00002);

        PAssert.IsTrue(() => actual == 0x78);
    }

    [Fact]
    public void TestPeekWord()
    {
        var actual = _target.PeekWord(0x00002);

        PAssert.IsTrue(() => actual == 0x5678);
    }
}