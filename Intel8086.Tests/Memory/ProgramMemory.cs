﻿using Intel8086.Binary;
using Intel8086.Register;

namespace Intel8086.Memory;

public sealed class ProgramMemory
{
    private readonly CPUMemory _memory;
    private readonly Registers _registers;

    public ProgramMemory(Registers registers, CPUMemory memory)
    {
        _registers = registers;
        _memory = memory;
    }

    private Address Address()
    {
        return new Address(_registers.CS, _registers.PC);
    }

    public UnsignedByte Pop8()
    {
        var value = _memory.PeekByte(Address());
        _registers.PC.Increment();
        return value;
    }

    public UnsignedWord Pop16()
    {
        var low = Pop8();
        var high = Pop8();

        return (ushort) (high.ToByte() * 0x100 + low.ToByte());
    }
}
