﻿namespace Intel8086.Memory;

public abstract partial class OffsetAddress
{
    internal sealed class ImmediateOffsetAddress : OffsetAddress
    {
        private readonly ushort _value;

        public ImmediateOffsetAddress(ushort value)
        {
            _value = value;
        }

        public override uint ToUInt16()
        {
            return _value;
        }
    }
}