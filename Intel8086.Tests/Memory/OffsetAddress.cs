﻿using Intel8086.Register;

namespace Intel8086.Memory;

public abstract partial class OffsetAddress
{
    public static implicit operator OffsetAddress(RegisterWord value)
    {
        return new RegisterOffsetAddress(value);
    }

    public static implicit operator OffsetAddress(ushort value)
    {
        return new ImmediateOffsetAddress(value);
    }

    public static OffsetAddress Combined(RegisterWord register1, RegisterWord register2)
    {
        return new CombinedOffsetAddress(register1, register2);
    }

    private sealed class CombinedOffsetAddress : OffsetAddress
    {
        private readonly RegisterOffsetAddress _register1;
        private readonly RegisterOffsetAddress _register2;

        public CombinedOffsetAddress(RegisterWord register1, RegisterWord register2)
        {
            _register1 = new RegisterOffsetAddress(register1);
            _register2 = new RegisterOffsetAddress(register2);
        }

        public override uint ToUInt16()
        {
            return _register1.ToUInt16() + _register2.ToUInt16();
        }
    }

    public abstract uint ToUInt16();
}