﻿using System;
using Intel8086.Binary;
using Intel8086.Register;

namespace Intel8086.Memory;

public readonly struct Address : IEquatable<Address>
{
    private readonly uint _value;

    public Address(SegmentRegister segment, OffsetAddress offset) :
        this(segment.AddressBase() + offset.ToUInt16())
    {
    }

    private Address(uint value)
    {
        _value = value;
    }

    public uint ToUInt()
    {
        // TODO lazy binding.
        return _value;
    }

    public override string ToString()
    {
        return $"{_value:x05}";
    }

    public bool Equals(Address other)
    {
        return _value == other._value;
    }

    public override bool Equals(object? obj)
    {
        return obj is Address other && Equals(other);
    }

    public override int GetHashCode()
    {
        return (int)_value;
    }

    public static bool operator ==(Address left, Address right)
    {
        return left.Equals(right);
    }

    public static bool operator !=(Address left, Address right)
    {
        return !left.Equals(right);
    }

    public static implicit operator Address(uint value)
    {
        return new Address(value);
    }

    public Address Increment()
    {
        return new Address(_value + 1);
    }

    public Address Add(SignedByte value)
    {
        return new Address((uint)(_value + value.ToSByte()));
    }
}