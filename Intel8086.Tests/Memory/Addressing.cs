﻿using System;
using System.Collections.Immutable;
using System.Linq;
using Intel8086.Binary;
using Intel8086.Store;

namespace Intel8086.Memory;

public sealed class Addressing : ISource<UnsignedWord>
{
    private readonly Func<CPU, Address> _address;
    private readonly string _label;

    private Addressing(Func<CPU, Address> address, UnsignedByte code, string label)
    {
        _address = address;
        _label = label;
        Code = code;
    }

    public UnsignedByte Code { get; }

    public static Addressing BXSI { get; } =
        new(cpu => new Address(cpu.Registers.DS, OffsetAddress.Combined(cpu.Registers.BX, cpu.Registers.SI)), 0b000,
            "[BX+SI]");

    public static Addressing BXDI { get; } =
        new(cpu => new Address(cpu.Registers.DS, OffsetAddress.Combined(cpu.Registers.BX, cpu.Registers.DI)), 0b001,
            "[BX+DI]");

    public static Addressing BPSI { get; } =
        new(cpu => new Address(cpu.Registers.SS, OffsetAddress.Combined(cpu.Registers.BP, cpu.Registers.SI)), 0b010,
            "[BP+SI]");

    public static Addressing BPDI { get; } =
        new(cpu => new Address(cpu.Registers.SS, OffsetAddress.Combined(cpu.Registers.BP, cpu.Registers.DI)), 0b011,
            "[BP+DI]");

    public static Addressing SI { get; } = new(cpu => new Address(cpu.Registers.DS, cpu.Registers.SI), 0b100, "[SI]");
    public static Addressing DI { get; } = new(cpu => new Address(cpu.Registers.DS, cpu.Registers.DI), 0b101, "[DI]");
    public static Addressing BP { get; } = new(cpu => new Address(cpu.Registers.SS, cpu.Registers.BP), 0b110, "[BP]");
    public static Addressing BX { get; } = new(cpu => new Address(cpu.Registers.DS, cpu.Registers.BX), 0b111, "[BX]");

    private static ImmutableArray<Addressing> All { get; } = new[]
    {
        BXSI,
        BXDI,
        BPSI,
        BPDI,
        SI,
        DI,
        BP,
        BX
    }.ToImmutableArray();


    UnsignedWord ISource<UnsignedWord>.GetValue(CPU cpu)
    {
        return cpu.Memory.Cell(Address(cpu)).GetValue(cpu);
    }

    private Address Address(CPU cpu)
    {
        return _address(cpu);
    }

    public override string ToString()
    {
        return _label;
    }

    public static Addressing Get(UnsignedByte b)
    {
        return All.First(a => a.Code == b);
    }

    public Addressing Disp8(SignedByte value)
    {
        return new Addressing(
            cpu => _address(cpu).Add(value),
            Code,
            $"{value}{this}");
    }
}