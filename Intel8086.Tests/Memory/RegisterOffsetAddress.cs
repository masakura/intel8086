﻿using Intel8086.Register;

namespace Intel8086.Memory;

public abstract partial class OffsetAddress
{
    private sealed class RegisterOffsetAddress : OffsetAddress
    {
        private readonly RegisterWord _register;

        public RegisterOffsetAddress(RegisterWord register)
        {
            _register = register;
        }

        public override uint ToUInt16()
        {
            return _register.Value.ToUInt16();
        }
    }
}