﻿using Intel8086.Operations.Add;
using Intel8086.Operations.Int;
using Intel8086.Operations.Mov;
using Intel8086.Operations.Parsing;

namespace Intel8086.Operations;

public sealed class OperationsParser : ParserCollection
{
    public OperationsParser() : base(new IParser[]
    {
        new AddParser(),
        new IntParser(),
        new MovParser()
    })
    {
    }
}