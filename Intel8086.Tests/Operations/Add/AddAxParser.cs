﻿using Intel8086.Binary;
using Intel8086.Memory;
using Intel8086.Operations.Parsing;

namespace Intel8086.Operations.Add;

public sealed class AddAxParser : IParser
{
    public IOperation? Parse(UnsignedByte b, ProgramMemory programMemory)
    {
        if (b == 0x05) return new AddAxOperation(programMemory.Pop16());

        return null;
    }
}