﻿using Intel8086.Operations.Parsing;

namespace Intel8086.Operations.Add;

public sealed class AddParser : ParserCollection
{
    public AddParser() : base(new IParser[]
    {
        new AddAxParser()
    })
    {
    }
}