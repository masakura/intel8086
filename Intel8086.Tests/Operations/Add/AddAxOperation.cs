﻿using Intel8086.Binary;

namespace Intel8086.Operations.Add;

public sealed class AddAxOperation : IOperation
{
    private readonly UnsignedWord _value;

    public AddAxOperation(UnsignedWord value)
    {
        _value = value;
    }

    public void Run(CPU cpu)
    {
        cpu.Registers.AX.Add(_value);
    }
}