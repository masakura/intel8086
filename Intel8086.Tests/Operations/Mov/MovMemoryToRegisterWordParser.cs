using System;
using Intel8086.Binary;
using Intel8086.Memory;
using Intel8086.Operations.Parsing;

namespace Intel8086.Operations.Mov;

internal sealed class MovMemoryToRegisterWordParser : IParser
{
    public IOperation? Parse(UnsignedByte b, ProgramMemory programMemory)
    {
        if (b != 0x8b) return null;

        var modRegRm = ModRegRm.Parse(programMemory.Pop8());

        return MovOperation.Create(modRegRm.Reg, modRegRm.Addressing(programMemory));
    }
}