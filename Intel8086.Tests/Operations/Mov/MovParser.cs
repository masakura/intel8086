﻿using Intel8086.Operations.Parsing;

namespace Intel8086.Operations.Mov;

public sealed class MovParser : ParserCollection
{
    public MovParser() : base(new IParser[]
    {
        new MovImmediateToRegisterParser(),
        new MovMemoryToRegisterWordParser()
    })
    {
    }
}