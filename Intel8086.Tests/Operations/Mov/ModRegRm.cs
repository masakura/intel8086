﻿using Intel8086.Binary;
using Intel8086.Memory;
using Intel8086.Operations.Parsing.Register;

namespace Intel8086.Operations.Mov;

internal readonly struct ModRegRm
{
    private Mod Mod { get; }
    public RegWord Reg { get; }
    private Addressing Rm { get; }

    public ModRegRm(Mod mod, RegWord reg, Addressing rm)
    {
        Mod = mod;
        Reg = reg;
        Rm = rm;
    }

    public static ModRegRm Parse(UnsignedByte b)
    {
        return new ModRegRm(
            (Mod)(b.And(0b1100_0000) >> 6).ToByte(),
            RegWord.Get(b.And(0b0011_1000) >> 3),
            Memory.Addressing.Get(b.And(0b111))
        );
    }

    private byte ToByte()
    {
        return (byte)((((byte)Mod & 0b11) << 6) |
                      (Reg.Code.ToByte() << 3) |
                      Rm.Code.ToByte());
    }

    public static implicit operator byte(ModRegRm value)
    {
        return value.ToByte();
    }

    public Addressing Addressing(ProgramMemory memory)
    {
        return Mod.Addressing(Rm, memory);
    }
}