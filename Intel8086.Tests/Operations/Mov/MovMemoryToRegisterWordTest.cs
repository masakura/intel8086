using System.Collections.Generic;
using System.Linq;
using Intel8086.Memory;
using Intel8086.Operations.Parsing.Register;
using Intel8086.Testing;
using PowerAssert;
using Xunit;

namespace Intel8086.Operations.Mov;

public sealed class MovMemoryToRegisterWordTest
{
    [Fact]
    public void TestMoveMemoryToRegisterBXSI()
    {
        var target = new CPUBuilder()
            .DataSegment(0x1000, new byte[] { 0x34, 0x12, 0x78, 0x56, 0xbc, 0x9a })
            .Program(new byte[] { 0x8b, new ModRegRm(0, RegWord.AX, Addressing.BXSI) })
            .Registers(registers =>
            {
                registers.BX.Value = 0x0001;
                registers.SI.Value = 0x0002;
            })
            .Build();

        target.RunOne();

        var actual = RegWord.AX.RegisterValue(target);

        PAssert.IsTrue(() => actual == 0xbc56);
    }

    [Fact]
    public void TestMoveMemoryToRegisterBXDI()
    {
        var target = new CPUBuilder()
            .DataSegment(0x1000, new byte[] { 0x34, 0x12, 0x78, 0x56, 0xbc, 0x9a })
            .Program(new byte[] { 0x8b, new ModRegRm(0, RegWord.AX, Addressing.BXDI) })
            .Registers(registers =>
            {
                registers.BX.Value = 0x0001;
                registers.DI.Value = 0x0002;
            })
            .Build();

        target.RunOne();

        var actual = RegWord.AX.RegisterValue(target);

        PAssert.IsTrue(() => actual == 0xbc56);
    }

    [Fact]
    public void TestMoveMemoryToRegisterBPSI()
    {
        var target = new CPUBuilder()
            .StackSegment(0x1000, new byte[] { 0x34, 0x12, 0x78, 0x56, 0xbc, 0x9a })
            .Program(new byte[] { 0x8b, new ModRegRm(0, RegWord.AX, Addressing.BPSI) })
            .Registers(registers =>
            {
                registers.BP.Value = 0x0001;
                registers.SI.Value = 0x0002;
            })
            .Build();

        target.RunOne();

        var actual = RegWord.AX.RegisterValue(target);

        PAssert.IsTrue(() => actual == 0xbc56);
    }

    [Fact]
    public void TestMoveMemoryToRegisterBPDI()
    {
        var target = new CPUBuilder()
            .StackSegment(0x1000, new byte[] { 0x34, 0x12, 0x78, 0x56, 0xbc, 0x9a })
            .Program(new byte[] { 0x8b, new ModRegRm(0, RegWord.AX, Addressing.BPDI) })
            .Registers(registers =>
            {
                registers.BP.Value = 0x0001;
                registers.DI.Value = 0x0002;
            })
            .Build();

        target.RunOne();

        var actual = RegWord.AX.RegisterValue(target);

        PAssert.IsTrue(() => actual == 0xbc56);
    }

    [Fact]
    public void TestMoveMemoryToRegisterSI()
    {
        var target = new CPUBuilder()
            .DataSegment(0x1000, new byte[] { 0x34, 0x12, 0x78, 0x56 })
            .Program(new byte[] { 0x8b, new ModRegRm(0, RegWord.AX, Addressing.SI) })
            .Registers(registers => registers.SI.Value = 0x0002)
            .Build();

        target.RunOne();

        var actual = RegWord.AX.RegisterValue(target);

        PAssert.IsTrue(() => actual == 0x5678);
    }

    [Fact]
    public void TestMoveMemoryToRegisterDI()
    {
        var target = new CPUBuilder()
            .DataSegment(0x1000, new byte[] { 0x34, 0x12, 0x78, 0x56 })
            .Program(new byte[] { 0x8b, new ModRegRm(0, RegWord.AX, Addressing.DI) })
            .Registers(registers => registers.DI.Value = 0x0002)
            .Build();

        target.RunOne();

        var actual = RegWord.AX.RegisterValue(target);

        PAssert.IsTrue(() => actual == 0x5678);
    }

    [Fact]
    public void TestMoveMemoryToRegisterBP()
    {
        var target = new CPUBuilder()
            .StackSegment(0x1000, new byte[] { 0x34, 0x12, 0x78, 0x56 })
            .Program(new byte[] { 0x8b, new ModRegRm(0, RegWord.AX, Addressing.BP) })
            .Registers(registers => registers.BP.Value = 0x0002)
            .Build();

        target.RunOne();

        var actual = RegWord.AX.RegisterValue(target);

        PAssert.IsTrue(() => actual == 0x5678);
    }

    [Theory]
    [MemberData(nameof(Source))]
    public void TestMoveMemoryToRegisterBX(RegWord destination)
    {
        var target = new CPUBuilder()
            .DataSegment(0x1000, new byte[] { 0x34, 0x12, 0x78, 0x56 })
            .Program(new byte[] { 0x8b, new ModRegRm(0, destination, Addressing.BX) })
            .Registers(registers => registers.BX.Value = 0x0002)
            .Build();

        target.RunOne();

        var actual = destination.RegisterValue(target);

        PAssert.IsTrue(() => actual == 0x5678);
    }

    private static IEnumerable<object> Source()
    {
        return RegWord.All.Select(reg => new object[] { reg });
    }
}