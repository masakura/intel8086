using Intel8086.Binary;
using Intel8086.Memory;
using Intel8086.Operations.Parsing;
using Intel8086.Operations.Parsing.Register;

namespace Intel8086.Operations.Mov;

public sealed class MovImmediateToRegisterParser : IParser
{
    public IOperation? Parse(UnsignedByte b, ProgramMemory programMemory)
    {
        var codes = Codes.Parse(b);
        if (codes.Code != 0b10110000) return null;

        if (codes.ByteOrWord == ByteOrWord.Word)
            return MovOperation.Create(RegWord.Get(codes.Register), programMemory.Pop16());

        if (codes.ByteOrWord == ByteOrWord.Byte)
            return MovOperation.Create(RegByte.Get(codes.Register), programMemory.Pop8());

        return null;
    }

    private struct Codes
    {
        public UnsignedByte Code { get; }
        public ByteOrWord ByteOrWord { get; }
        public UnsignedByte Register { get; }

        private Codes(UnsignedByte code, ByteOrWord byteOrWord, UnsignedByte register)
        {
            Code = code;
            ByteOrWord = byteOrWord;
            Register = register;
        }

        public static Codes Parse(UnsignedByte b)
        {
            return new Codes(
                b.And(0b11110000),
                ParseByteOrWord(b),
                b.And(0b111)
            );
        }

        private static ByteOrWord ParseByteOrWord(UnsignedByte b)
        {
            if (b.And(0b00001000) >> 3 != 0) return ByteOrWord.Word;
            return ByteOrWord.Byte;
        }
    }

    private enum ByteOrWord
    {
        Byte = 0,
        Word = 1
    }
}
