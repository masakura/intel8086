﻿namespace Intel8086.Operations.Mov;

internal enum Mod
{
    NoDisp = 0b00,
    Disp8 = 0b01
}