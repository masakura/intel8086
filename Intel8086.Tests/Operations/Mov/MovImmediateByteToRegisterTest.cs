using System.Collections.Generic;
using System.Linq;
using Intel8086.Operations.Parsing.Register;
using Intel8086.Testing;
using PowerAssert;
using Xunit;

namespace Intel8086.Operations.Mov;

public sealed class MovImmediateByteToRegisterTest
{
    [Theory]
    [MemberData(nameof(Source))]
    public void TestMovImmediateByteToRegister(RegByte destination)
    {
        var target = new CPUBuilder()
            // MOV reg, 0x10
            .Program(new byte[] {(destination.Code | 0xb0).ToByte(), 0x10})
            .Build();

        target.RunOne();

        var actual = destination.RegisterValue(target);

        PAssert.IsTrue(() => actual == 0x10);
    }

    private static IEnumerable<object> Source()
    {
        return RegByte.All.Select(register => new object[] {register});
    }
}