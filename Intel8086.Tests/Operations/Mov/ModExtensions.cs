﻿using System;
using Intel8086.Memory;

namespace Intel8086.Operations.Mov;

internal static class ModExtensions
{
    public static Addressing Addressing(this Mod mod, Addressing rm, ProgramMemory memory)
    {
        return mod switch
        {
            Mod.NoDisp => rm,
            Mod.Disp8 => rm.Disp8(memory.Pop8().Signed()),
            _ => throw new InvalidOperationException("Unsupported mod")
        };
    }
}