﻿using System.Collections.Generic;
using System.Linq;
using Intel8086.Operations.Parsing.Register;
using Intel8086.Testing;
using PowerAssert;
using Xunit;

namespace Intel8086.Operations.Mov;

public sealed class MovImmediateWordToRegisterTest
{
    [Theory]
    [MemberData(nameof(Source))]
    public void TestMovImmediateToRegisterWord(RegWord destination)
    {
        var target = new CPUBuilder()
            // MOV reg, 0x0100
            .Program(new byte[] {(0xb8 | destination.Code).ToByte(), 0x00, 0x01})
            .Build();

        target.RunOne();

        var actual = destination.RegisterValue(target);

        PAssert.IsTrue(() => actual == 0x0100);
    }

    private static IEnumerable<object> Source()
    {
        return RegWord.All.Select(reg => new object[] {reg});
    }
}