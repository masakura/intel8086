﻿using Intel8086.Memory;
using Intel8086.Operations.Parsing.Register;
using Intel8086.Testing;
using PowerAssert;
using Xunit;

namespace Intel8086.Operations.Mov;

/// <summary>
///     MOV AX, 8[BX]
/// </summary>
public sealed class MovMemoryToRegisterWordDisp8Test
{
    [Fact]
    public void TestOffsetPlus()
    {
        var target = new CPUBuilder()
            .DataSegment(0x1000, new byte[] { 0x34, 0x12, 0x78, 0x56, 0xbc, 0x9a })
            .Program(new byte[]
            {
                0x8b,
                new ModRegRm(Mod.Disp8, RegWord.AX, Addressing.BX),
                0x01
            })
            .Registers(registers => registers.BX.Value = 0x0002)
            .Build();

        target.RunOne();

        var actual = RegWord.AX.RegisterValue(target);

        PAssert.IsTrue(() => actual == 0xbc56);
    }

    [Fact]
    public void TestOffsetMinus()
    {
        var target = new CPUBuilder()
            .DataSegment(0x1000, new byte[] { 0x34, 0x12, 0x78, 0x56, 0xbc, 0x9a })
            .Program(new byte[]
            {
                0x8b,
                new ModRegRm(Mod.Disp8, RegWord.AX, Addressing.BX),
                255 // -1
            })
            .Registers(registers => registers.BX.Value = 0x0002)
            .Build();

        target.RunOne();

        var actual = RegWord.AX.RegisterValue(target);

        PAssert.IsTrue(() => actual == 0x7812);
    }
}