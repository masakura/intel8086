﻿using Intel8086.Operations.Parsing.Register;
using Intel8086.Store;

namespace Intel8086.Operations.Mov;

internal static class MovOperation
{
    public static IOperation Create<T>(Reg<T> destination, T source)
    {
        return new MovOperation<T>(destination, new ImmediateSource<T>(source));
    }

    public static IOperation Create<T>(Reg<T> destination, ISource<T> source)
    {
        return new MovOperation<T>(destination, source);
    }
}

internal sealed class MovOperation<T> : IOperation
{
    private readonly IDestination<T> _destination;
    private readonly ISource<T> _source;

    public MovOperation(IDestination<T> destination, ISource<T> source)
    {
        _destination = destination;
        _source = source;
    }

    public void Run(CPU cpu)
    {
        _destination.SetValue(cpu, _source.GetValue(cpu));
    }
}