﻿using Intel8086.Binary;
using Intel8086.Memory;
using Intel8086.Operations.Parsing;

namespace Intel8086.Operations.Int;

internal sealed class Int3Parser : IParser
{
    public IOperation? Parse(UnsignedByte b, ProgramMemory programMemory)
    {
        if (b == 0xcc) return new Int3Operation();
        return null;
    }
}