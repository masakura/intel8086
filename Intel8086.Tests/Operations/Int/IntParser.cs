﻿using Intel8086.Operations.Parsing;

namespace Intel8086.Operations.Int;

public sealed class IntParser : ParserCollection
{
    public IntParser() : base(new IParser[] {new Int3Parser()})
    {
    }
}