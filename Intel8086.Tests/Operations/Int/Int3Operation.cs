﻿namespace Intel8086.Operations.Int;

public sealed class Int3Operation : IOperation
{
    public void Run(CPU cpu)
    {
        throw new Int3Exception();
    }
}