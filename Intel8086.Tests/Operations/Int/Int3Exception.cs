﻿using System;

namespace Intel8086.Operations.Int;

public sealed class Int3Exception : Exception
{
    public Int3Exception() : base("INT3")
    {
    }
}