﻿using Intel8086.Binary;
using Intel8086.Memory;

namespace Intel8086.Operations.Parsing;

public interface IParser
{
    IOperation? Parse(UnsignedByte b, ProgramMemory programMemory);
}