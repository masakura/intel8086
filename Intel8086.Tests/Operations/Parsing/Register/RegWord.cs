using System;
using System.Collections.Immutable;
using Intel8086.Binary;
using Intel8086.Register;

namespace Intel8086.Operations.Parsing.Register;

public sealed class RegWord : Reg<UnsignedWord>
{
    private RegWord(string name, 
        UnsignedByte code,
        Func<Registers, IRegister<UnsignedWord>> register) :
        base(name, code, register)
    {
    }

    public static RegWord AX { get; } = new(nameof(AX), 0b000, registers => registers.AX);
    public static RegWord CX { get; } = new(nameof(CX), 0b001, registers => registers.CX);
    public static RegWord DX { get; } = new(nameof(DX), 0b010, registers => registers.DX);
    public static RegWord BX { get; } = new(nameof(BX), 0b011, registers => registers.BX);
    public static RegWord SP { get; } = new(nameof(SP), 0b100, registers => registers.SP);
    public static RegWord BP { get; } = new(nameof(BP), 0b101, registers => registers.BP);
    public static RegWord SI { get; } = new(nameof(SI), 0b110, registers => registers.SI);
    public static RegWord DI { get; } = new(nameof(DI), 0b111, registers => registers.DI);

    public static ImmutableArray<RegWord> All { get; } = new[]
    {
        AX,
        CX,
        DX,
        BX,
        SP,
        BP,
        SI,
        DI
    }.ToImmutableArray();

    private static ImmutableDictionary<UnsignedByte, RegWord> Map { get; } =
        All.ToImmutableDictionary(reg => reg.Code, reg => reg);

    public static RegWord Get(UnsignedByte code)
    {
        if (Map.TryGetValue(code, out var value)) return value;
        throw new ParseException("Unknown register.");
    }
}