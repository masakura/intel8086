using System.Collections.Generic;
using Intel8086.Binary;
using Intel8086.Operations.Parsing.Register.Testing;
using Intel8086.Register;
using PowerAssert;
using Xunit;

namespace Intel8086.Operations.Parsing.Register;

public sealed class RegByteTest
{
    private readonly CPU _cpu;
    private readonly Registers _registers;

    public RegByteTest()
    {
        _cpu = new CPU();
        _registers = _cpu.Registers;
    }

    [Theory]
    [MemberData(nameof(Source))]
    public void TestGet(UnsignedByte code, Register<UnsignedByte> register)
    {
        var target = RegByte.Get(code);

        var actual = new {target.Code, Register = target.Register(_registers)};
        var expected = new {Code = code, Register = register.Get(_registers)};

        PAssert.IsTrue(() => actual.Equals(expected));
    }

    [Fact]
    public void TestGetNotFound()
    {
        PAssert.Throws<ParseException>(() => RegByte.Get(0b1000));
    }

    [Fact]
    public void TestRegisterValue()
    {
        _registers.CH.Value = 0x12;

        var actual = RegByte.CH.RegisterValue(_cpu);

        PAssert.IsTrue(() => actual == 0x12);
    }

    private static IEnumerable<object> Source()
    {
        yield return new object[] {new UnsignedByte(0b000), Testing.Register.Byte("AL", registers => registers.AL)};
        yield return new object[] {new UnsignedByte(0b001), Testing.Register.Byte("CL", registers => registers.CL)};
        yield return new object[] {new UnsignedByte(0b010), Testing.Register.Byte("DL", registers => registers.DL)};
        yield return new object[] {new UnsignedByte(0b011), Testing.Register.Byte("BL", registers => registers.BL)};
        yield return new object[] {new UnsignedByte(0b100), Testing.Register.Byte("AH", registers => registers.AH)};
        yield return new object[] {new UnsignedByte(0b101), Testing.Register.Byte("CH", registers => registers.CH)};
        yield return new object[] {new UnsignedByte(0b110), Testing.Register.Byte("DH", registers => registers.DH)};
        yield return new object[] {new UnsignedByte(0b111), Testing.Register.Byte("BH", registers => registers.BH)};
    }
}