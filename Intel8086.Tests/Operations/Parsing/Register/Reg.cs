using System;
using Intel8086.Binary;
using Intel8086.Register;
using Intel8086.Store;

namespace Intel8086.Operations.Parsing.Register;

public abstract class Reg<T> : IDestination<T>
{
    private readonly string _name;
    private readonly Func<Registers, IRegister<T>> _register;

    protected Reg(string name, UnsignedByte code, Func<Registers, IRegister<T>> register)
    {
        _name = name;
        _register = register;
        Code = code;
    }

    public UnsignedByte Code { get; }

    void IDestination<T>.SetValue(CPU cpu, T value)
    {
        Register(cpu.Registers).Value = value;
    }

    public IRegister<T> Register(Registers registers)
    {
        return _register(registers);
    }

    public T RegisterValue(CPU cpu)
    {
        return Register(cpu.Registers).Value;
    }

    public override string ToString()
    {
        return _name;
    }
}