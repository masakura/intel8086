using System;
using System.Collections.Immutable;
using Intel8086.Binary;
using Intel8086.Register;

namespace Intel8086.Operations.Parsing.Register;

public sealed class RegByte : Reg<UnsignedByte>
{
    private RegByte(string name, UnsignedByte code, Func<Registers, IRegister<UnsignedByte>> register) : base(name,
        code, register)
    {
    }

    public static RegByte AL { get; } = new(nameof(AL), 0b000, registers => registers.AL);
    public static RegByte CL { get; } = new(nameof(CL), 0b001, registers => registers.CL);
    public static RegByte DL { get; } = new(nameof(DL), 0b010, registers => registers.DL);
    public static RegByte BL { get; } = new(nameof(BL), 0b011, registers => registers.BL);
    public static RegByte AH { get; } = new(nameof(AH), 0b100, registers => registers.AH);
    public static RegByte CH { get; } = new(nameof(CH), 0b101, registers => registers.CH);
    public static RegByte DH { get; } = new(nameof(DH), 0b110, registers => registers.DH);
    public static RegByte BH { get; } = new(nameof(BH), 0b111, registers => registers.BH);

    public static ImmutableArray<RegByte> All { get; } = new[]
    {
        AL,
        CL,
        DL,
        BL,
        AH,
        CH,
        DH,
        BH
    }.ToImmutableArray();

    private static ImmutableDictionary<UnsignedByte, RegByte> Map { get; } =
        All.ToImmutableDictionary(reg => reg.Code, reg => reg);

    public static RegByte Get(UnsignedByte code)
    {
        if (Map.TryGetValue(code, out var value)) return value;
        throw new ParseException("Unknown register.");
    }
}