using System;
using Intel8086.Binary;
using Intel8086.Register;

namespace Intel8086.Operations.Parsing.Register.Testing;

public sealed class Register<T>
{
    private readonly string _name;
    private readonly Func<Registers, IRegister<T>> _register;

    public Register(string name, Func<Registers, IRegister<T>> register)
    {
        _name = name;
        _register = register;
    }

    public IRegister<T> Get(Registers registers)
    {
        return _register(registers);
    }

    public override string ToString()
    {
        return _name;
    }
}

public static class Register
{
    public static Register<UnsignedWord> Word(string name, Func<Registers, IRegister<UnsignedWord>> register)
    {
        return new Register<UnsignedWord>(name, register);
    }

    public static Register<UnsignedByte> Byte(string name, Func<Registers, IRegister<UnsignedByte>> register)
    {
        return new Register<UnsignedByte>(name, register);
    }
}