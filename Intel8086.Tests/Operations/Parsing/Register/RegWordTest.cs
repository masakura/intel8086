using System.Collections.Generic;
using Intel8086.Binary;
using Intel8086.Register;
using PowerAssert;
using Xunit;

namespace Intel8086.Operations.Parsing.Register;

public sealed class RegWordTest
{
    private readonly Registers _registers;
    private readonly CPU _cpu;

    public RegWordTest()
    {
        _cpu = new CPU();
        _registers = _cpu.Registers;
    }

    [Theory]
    [MemberData(nameof(Source))]
    public void TestGet(UnsignedByte code, Testing.Register<UnsignedWord> register)
    {
        var target = RegWord.Get(code);

        var actual = new {target.Code, Register = target.Register(_registers)};
        var expected = new {Code = code, Register = register.Get(_registers)};

        PAssert.IsTrue(() => actual.Equals(expected));
    }

    [Fact]
    public void TestGetNotFound()
    {
        PAssert.Throws<ParseException>(() => RegWord.Get(0b1000));
    }

    [Fact]
    public void TestRegisterValue()
    {
        _registers.CX.Value = 0x1234;

        var actual = RegWord.CX.RegisterValue(_cpu);
        
        PAssert.IsTrue(() => actual == 0x1234);
    }

    private static IEnumerable<object> Source()
    {
        yield return new object[] {new UnsignedByte(0b000), Testing.Register.Word("AX", registers => registers.AX)};
        yield return new object[] {new UnsignedByte(0b001), Testing.Register.Word("CX", registers => registers.CX)};
        yield return new object[] {new UnsignedByte(0b010), Testing.Register.Word("DX", registers => registers.DX)};
        yield return new object[] {new UnsignedByte(0b011), Testing.Register.Word("BX", registers => registers.BX)};
        yield return new object[] {new UnsignedByte(0b100), Testing.Register.Word("SP", registers => registers.SP)};
        yield return new object[] {new UnsignedByte(0b101), Testing.Register.Word("BP", registers => registers.BP)};
        yield return new object[] {new UnsignedByte(0b110), Testing.Register.Word("SI", registers => registers.SI)};
        yield return new object[] {new UnsignedByte(0b111), Testing.Register.Word("DI", registers => registers.DI)};
    }
}