﻿using System;

namespace Intel8086.Operations.Parsing;

public sealed class ParseException : Exception
{
    public ParseException(string message) : base(message)
    {
    }
}