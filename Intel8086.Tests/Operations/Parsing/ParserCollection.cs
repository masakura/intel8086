﻿using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using Intel8086.Binary;
using Intel8086.Memory;

namespace Intel8086.Operations.Parsing;

public class ParserCollection : IParser
{
    private readonly ImmutableArray<IParser> _parsers;

    protected ParserCollection(IEnumerable<IParser> parsers)
    {
        _parsers = parsers.ToImmutableArray();
    }

    public IOperation? Parse(UnsignedByte b, ProgramMemory programMemory)
    {
        return _parsers
            .Select(parser => parser.Parse(b, programMemory))
            .FirstOrDefault(operation => operation != null);
    }
}