﻿// ReSharper disable once CheckNamespace
namespace Intel8086.Operations;

public interface IOperation
{
    void Run(CPU cpu);
}