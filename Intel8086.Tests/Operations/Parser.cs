﻿using Intel8086.Memory;
using Intel8086.Operations.Parsing;

namespace Intel8086.Operations;

public sealed class Parser
{
    private readonly IParser _parser = new OperationsParser();

    public IOperation Parse(ProgramMemory programMemory)
    {
        var b = programMemory.Pop8();

        return _parser.Parse(b, programMemory) ?? throw new ParseException("パースできません。");
    }
}